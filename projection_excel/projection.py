import json
import uuid
from json import dumps, loads

import boto3
import numpy as np
import openpyxl
import requests

client = boto3.resource('dynamodb', endpoint_url='http://localhost:8000')
table_template = client.Table('template')

excel_file = openpyxl.load_workbook('sv_task_temp.xlsx')
templates_json_list = excel_file['sv_task_temp']


def readFileExcel():
    counter = 0
    for template_row in range(2, templates_json_list.max_row + 1):
        template_id = templates_json_list.cell(template_row, 1).value
        created_by = templates_json_list.cell(template_row, 3).value
        name = templates_json_list.cell(template_row, 4).value
        description = templates_json_list.cell(template_row, 5).value
        icon = templates_json_list.cell(template_row, 6).value
        custom = templates_json_list.cell(template_row, 9)

        if custom.value is not None:
            section = {
                "custom": {"id": str(uuid.uuid4()).split('-')[0], "inputs": []},
                "procedures": [],
                "aprovements": []
            }
            json_obj = json.loads(dumps(loads(custom.value), separators=(',', ':')))
            array_inputs = np.array(json_obj)
            for input_custom in array_inputs:
                custom_input = {
                    **input_custom,
                    "id": str(uuid.uuid4()).split('-')[0]
                }

                # Convert values of select
                if custom_input['type'] == 'select':
                    transform_data_values(custom_input)

                if custom_input['type'] == 'radio-group':
                    transform_data_values(custom_input)

                if custom_input['type'] == 'checkbox-group':
                    transform_data_values(custom_input)

                section['custom']['inputs'].append(custom_input)
        else:
            section = {
                "procedures": [],
                "aprovements": []
            }

        template = {
            "id": f"{template_id}",
            "name": name if name else f"Nombre no definido - {template_id}",
            "type": 'template',
            "description": description if name else " ",
            "icon": icon if icon else " ",
            "createdById": f"{created_by}",
            "createdByName": "CesarGlez",
            "sections": dumps(section),
        }

        print(template)
        counter += 1
        url = 'http://localhost:3000/api/v1/templates'
        response = requests.post(url, json=template)
        print(f'Response {response.content}')
        counter += 1


def transform_data_values(custom_input):
    current_input_values = np.array(custom_input['values'])
    current_values = []

    for value in current_input_values:
        current_value = {
            **value,
            "id": str(uuid.uuid4()).split('-')[0]
        }
        current_values.append(current_value)

    custom_input['values'] = current_values
    return custom_input


def readCustomFromExcel():
    for template_row in range(2, templates_json_list.max_row + 1):
        custom = templates_json_list.cell(template_row, 9)
        if custom.value is not None:
            custom_row = {
                "id": str(uuid.uuid4()).split('-')[0],
                "inputs": []
            }
            json_objss = json.loads(dumps(loads(custom.value), separators=(',', ':')))
            array_inputs = np.array(json_objss)
            for input_custom in array_inputs:
                custom_input = {
                    **input_custom,
                    "id": str(uuid.uuid4()).split('-')[0]
                }
                custom_row['inputs'].append(custom_input)

            print(custom_row)

            # my_array = np.array(dumps(loads(custom.value), separators=(',', ':')))
            # print(my_array)


if __name__ == '__main__':
    readFileExcel()
