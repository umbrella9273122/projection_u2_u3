## REQUIREMENTS


## Create virtual environment

```
python3 -m venv env
```

## Activate virtual environment
```
source ./env/bin/activate
```

### Install dependencies with file requirements.txt

```
pip install -r requirements.txt
```

### Install dependencies manually

```
~ pip install mysql-connector-pyton
~ pip install numpy
~ pip install requests
```

## TABLES USED FROM U2 TO GENERATE JSON 

```
- virtual_row
- sv_task_temp
- sys_process
- bs_account
```


## Required fields || Optional

```

| Field           |          | Type Field |
|-----------------|----------|------------|
| id              | required | string     |
| name            | required | string     |
| type            | required | string     |
| siteId          | required | string     |
| createdAt       | optional | number     |
| updatedAt       | optional | number     |
| createdByName   | required | string     |
| createdById     | required | string     |
| icon            | optional | string     |
| description     | optional | string     |
| typeId          | required | string     |
| customStates    | optional | json       |
| sections        | optional | json       |
| profiles        | optional | json       |
| behavior        | optional | json       |
| app             | optional | json       |
| statusAvailable | optional | json       |
| status          | optional | string     |


```

## ENDPOINTS

### CREATE NEW TASK

```
curl --location 'http://u3dev.lavenirapps.co:3001/api/v1/templates/projection/task' \
--header 'Content-Type: application/json' \
--data '{
    "id": "15",
    "type": "task",
    "name": "Solicitud de Gesti\u00f3n Presupuestal",
    "description": "Generaci\u00f3n Solicitud de Pedido (SOLPEP)",
    "icon": "CheckCircleFilled",
    "createdById": "270",
    "createdByName": "Camila Andrea Ciclos Zuleta",
    "createdAt": 1616819450000,
    "updatedAt": 1682667389000,
    "siteId": "11",
    "typeId": "11#task",
    "sections": {
        "custom": {
            "id": "ac53a6d3",
            "inputs": [
                {
                    "type": "paragraph",
                    "label": "Generaci\u00f3n de requerimientos para crear solicitudes de pedido y realizar formatos de cumplimiento.<div>Por favor adjuntar formato</div>",
                    "id": "37592b0a"
                },
                {
                    "type": "select",
                    "required": true,
                    "label": "Tipo de Presupuesto",
                    "name": "tippresges",
                    "only": "0",
                    "values": [
                        {
                            "label": "",
                            "value": "",
                            "selected": false,
                            "id": "af2717e1"
                        },
                        {
                            "label": "CAPEX",
                            "value": "CAPEX",
                            "selected": true,
                            "id": "caac2cb9"
                        },
                        {
                            "label": "OPEX",
                            "value": "OPEX",
                            "selected": false,
                            "id": "26414e90"
                        }
                    ],
                    "id": "230be020",
                    "value": "CAPEX"
                },
                {
                    "type": "text",
                    "required": true,
                    "label": "Proyecto o Necesidad",
                    "name": "proynecgespres",
                    "only": "0",
                    "list": "0",
                    "column": "0",
                    "dcolumn": "0",
                    "value": "PROYECTO UMBRELLA",
                    "id": "f4598f3b"
                },
                {
                    "type": "textarea",
                    "label": "Observaciones",
                    "name": "obsergespres",
                    "maxlength": "600",
                    "only": "0",
                    "value": "prueba",
                    "id": "e1200096"
                },
                {
                    "type": "header",
                    "label": "Datos adjuntos ingresarlos en el campo de Anexos",
                    "id": "c6620faf"
                }
            ]
        },
        "procedures": [
            {
                "id": 778,
                "name": "Documento de Gesti\u00f3n"
            }
        ]
    }
}'
```